// [Section] Loop

/*
	Evaluate a certain condition. Is it is satisfied, it will repeat codes continuously.
	-Syntax:
		While(expression/condition){
			statement/s;
		}
*/

/*let count = 5;

while (count !== 0){
	console.log("While: " + count);
	count--;
}

let newCount = 10;

while (newCount <= 20){
	console.log(newCount);
	newCount++;
} */

// Do while Loop

/*
	Syntax:
		do{
			statement/s;
		}(condition)

*/

/*let number = Number(prompt("Give me a number"));

do{
	console.log("Do while: "+number);

	number += 1;
}while (number < 10)*/

/*let newNumber = Number(prompt("Give me another number"));

do{
	console.log(newNumber);

	newNumber -= 1;
}while (newNumber >= 20)*/

// For Loop

/*
	Syntax:
		for(initialization; condition; finalExpression)
*/

/*for (let countNew = 0; countNew <= 20; countNew++){
	console.log("For Loop: "+countNew);
}*/

let myString = "Aarush";

console.log("Total number of characters");
console.log(myString.length);  //Counts the number of characters that we have in the string

// console.log("Accessing elements in String through index");
// console.log(myString[0] + "  " + myString[1] + "  " + myString[2] + "  " + myString[3]);

console.log("Accessing elements in String through For loop.");
for(let indexNum = 0; indexNum < myString.length; indexNum++){
	console.log(myString[indexNum]);
}

let myName = "aaeeiioouu";

for(let x = 0; x < myName.length; x++){
	switch(myName[x].toLowerCase()){
		case 'a':
			console.log(3 + " " + myName[x]);
			break;
		case 'e':
			console.log(3 + " " + myName[x]);
			break;
		case 'i':
			console.log(3 + " " + myName[x]);
			break;
		case 'o':
			console.log(3 + " " + myName[x]);
			break;
		case 'u':
			console.log(3 + " " + myName[x]);
			break;
	}
}

// Continue and Break statements

console.log("Continue and Break")
for(let count = 0; count <= 20; count++){
	if(count%2 === 0){
		continue;   
	}

	console.log("Continue and Break: "+count);

	if(count > 10){
		break;
	}
}

console.log("Continue and Break using strings");

let name = "Aarush";

for(let i = 0; i < name.length; i++){

	if(name[i].toLowerCase() === 'a'){
		continue;
	}

	if(name[i].toLowerCase() === 'h'){
		break;
	}

	console.log(name[i]);
}
